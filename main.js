class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this._name = value;
    }

    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }

    get age() {
        return this._age;
    }

    set salary(value) {
        this._salary = value;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get lang() {
        return this._lang;
    }

    set salary(value) {
        super.salary = value;
    }

    get salary() {
        return super.salary * 3;
    }
}

const programmer = new Programmer("Pavlo", "22", "1000", "c#");
const programmer1 = new Programmer("Vitalii", "23", "1100", "js");
const programmer2 = new Programmer("Yuri", "24", "1200", "php");
console.log(programmer);
console.log(programmer1);
console.log(programmer2);